
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "unnax/version"

Gem::Specification.new do |spec|
  spec.name          = "unnax"
  spec.version       = Unnax::VERSION
  spec.authors       = ["Michael Skubenych"]
  spec.email         = ["michael.skubenych@gmail.com"]

  spec.summary       = %q{A simple wrapper around unnax.com}
  spec.description   = %q{A simple wrapper around unnax.com}
  spec.homepage      = "https://github.com/mskubenich/unnax"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "webmock", "~> 3.3"

  spec.add_dependency "rest-client"
  spec.add_dependency "json"
end
