require "unnax/version"
require "unnax/configuration"
require "unnax/requests/base"
require "unnax/requests/webhooks"
require "unnax/requests/payment_d_b_t"
require 'unnax/requests/reader_tokenized'

require "unnax/webhooks"
require "unnax/webhook"
require "unnax/bank"
require "unnax/bank_parameter"
require "unnax/account"
require 'unnax/job'

require "unnax/exceptions/base"
require "unnax/exceptions/unauthorized"
require "unnax/exceptions/unknown_error"
require "unnax/exceptions/validation_error"

module Unnax
  class << self
    attr_accessor :configuration

    def configure
      self.configuration ||= Unnax::Configuration.new
      yield(configuration)
    end
  end
end
