module Unnax
  class Account

    attr_accessor :id
    attr_accessor :account_number
    attr_accessor :flags
    attr_accessor :balance
    attr_accessor :label
    attr_accessor :currency
    attr_accessor :customers

    attr_accessor :parameters

    def initialize(*args)
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          case k
          when 'customers'
            @parameters = []
            v.each do |parameter|
              @parameters << Unnax::BankParameter.new(parameter)
            end
          else
            instance_variable_set :"@#{ k }", v
          end
        end
      end
    end
  end
end