module Unnax
  class Bank

    attr_accessor :id
    attr_accessor :mode
    attr_accessor :group_id
    attr_accessor :can_fast_transfer
    attr_accessor :group_name
    attr_accessor :prefixes
    attr_accessor :name
    attr_accessor :parameters

    attr_accessor :accounts

    def initialize(*args)
      @accounts = []
      
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          case k
          when 'parameters'
            @parameters = []
            v.each do |parameter|
              @parameters << Unnax::BankParameter.new(parameter)
            end
          else
            instance_variable_set :"@#{ k }", v
          end
        end
      end
    end
  end
end