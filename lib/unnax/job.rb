module Unnax
  class Job

    attr_accessor :job_id

    def initialize(*args)
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          instance_variable_set :"@#{ k }", v
        end
      end
    end
  end
end
