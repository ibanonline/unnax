module Unnax
  class AccountParameter

    attr_accessor :name
    attr_accessor :type
    attr_accessor :fields

    def initialize(*args)
      @fields = []

      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          if k == 'fields'
            v.each do |field|
              @fields << AccountField.new field
            end
          else
            instance_variable_set :"@#{ k }", v
          end
        end
      end
    end
  end
end
