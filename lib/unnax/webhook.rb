module Unnax
  class Webhook

    attr_accessor :id
    attr_accessor :client
    attr_accessor :event
    attr_accessor :target
    attr_accessor :state
    attr_accessor :created_at
    attr_accessor :updated_at

    def initialize(*args)
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          case k
          when 'created_at'
            @created_at = DateTime.parse(v)
          when 'updated_at'
            @updated_at = DateTime.parse(v)
          else
            instance_variable_set :"@#{ k }", v
          end
        end
      end
    end
  end
end