module Unnax
  class BankParameter

    attr_accessor :label
    attr_accessor :type
    attr_accessor :modes
    attr_accessor :optionsl
    attr_accessor :name
    attr_accessor :fields

    def initialize(*args)
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          instance_variable_set :"@#{ k }", v
        end
      end
    end
  end
end