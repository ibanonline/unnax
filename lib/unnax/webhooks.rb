module Unnax
  class Webhooks
    include Enumerable

    attr_accessor :count
    attr_accessor :next
    attr_accessor :previous
    attr_accessor :results

    def initialize(*args)
      @count = 0
      @results = []

      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          if k == 'results'
            v.each do |result|
              @results << Unnax::Webhook.new(result)
            end
          else
            instance_variable_set :"@#{ k }", v
          end
        end
      end
    end

    def each(&block)
      results.each(&block)
    end
  end
end