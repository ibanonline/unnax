module Unnax
  module Exceptions
    class ValidationError < Base
      attr_accessor :code
      attr_accessor :status
      attr_accessor :message
      attr_accessor :data

      def initialize(*args)
        if args.first.is_a?(Hash)
          args.first.each do |k, v|
            instance_variable_set :"@#{ k }", v
          end
        end
      end

      def to_s
        "@code=\"#{ @code }\" @status=\"#{ @status }\" @message=\"#{ @message }\" @data=\"#{ @data }\""
      end
    end
  end
end