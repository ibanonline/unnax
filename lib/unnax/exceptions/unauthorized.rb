module Unnax
  module Exceptions
    class Unauthorized < Base

      def initialize(message)
        @message = message
      end

      def to_s
        @message
      end
    end
  end
end