module Unnax
  module Requests
    class Webhooks < Base  
          
      def initialize
      
      end
    
      def self.list(options = {})
        # TODO state
        RestClient::Request.execute(
          method: :get,
          url: "#{ base_url }/webhooks/",
          headers: {
            'Authorization': authorization_header,
          }
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)          
          
          case result.code.to_i
          when 200
            return Unnax::Webhooks.new parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def self.create(options = {})
        RestClient::Request.execute(
          method: :post,
          url: "#{ base_url }/webhooks/",
          headers: {
            'Authorization': authorization_header,
            'Content-Type': 'application/json'
          },
          payload: {
            client: options[:email],
            event: options[:event],
            target: options[:target]
          }.to_json
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)

          case result.code.to_i
          when 200, 201
            return Unnax::Webhook.new parsed_response
          when 400
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end
    end
  end
end