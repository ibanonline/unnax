module Unnax
  module Requests
    class PaymentDBT < Base  
      
      attr_accessor :order_code
      attr_accessor :amount
      attr_accessor :bank_order_code
      attr_accessor :concept
      attr_accessor :customer_names
      attr_accessor :currency
      attr_accessor :callback_url
      attr_accessor :url_ok
      attr_accessor :url_ko
      attr_accessor :owners

      attr_accessor :sid
      attr_accessor :products

      attr_accessor :banks
      attr_accessor :bank

      attr_accessor :step2
      attr_accessor :retry
      attr_accessor :signature_option
      attr_accessor :parameters

      attr_accessor :account

      def initialize
      
      end

      def start(options = {})
    
        options[:order_code] ||= SecureRandom.hex

        options.each do |k, v|
          instance_variable_set :"@#{ k }", v
        end

        RestClient::Request.execute(
          method: :post,
          url: "#{ base_url }/payment/transfer/lockstep/init/",
          headers: {
            'Authorization': authorization_header,
            'Content-Type': 'application/json',
          },
          payload: options.to_json
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)  

          puts '---------------Start'
          puts response.code
          puts parsed_response                    
          
          case result.code.to_i
          when 200, 201
            parsed_response.each do |k, v|
              instance_variable_set :"@#{ k }", v
            end
          when 400
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def get_banks(options = {})
    
        options[:sid] ||= @sid

        RestClient::Request.execute(
          method: :get,
          url: "#{ base_url }/payment/transfer/lockstep/banks/?sid=#{ options[:sid] }"
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)          

          puts '---------------Banks'
          puts response.code
          puts parsed_response

          case result.code.to_i
          when 200, 201

            @banks = []

            parsed_response['banks'].each do |bank|
              @banks << Unnax::Bank.new(bank)
            end
          when 400
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def login(options = {})
    
        options[:sid] ||= @sid
        @bank = options[:bank]

        raise Unnax::Exceptions::UnknownError, "Please pass bank" unless @bank

        RestClient::Request.execute(
          method: :post,
          url: "#{ base_url }/payment/transfer/lockstep/login/?sid=#{ options[:sid] }",
          headers: {
            'Content-Type': 'application/json',
          },
          payload: {
            bank_id: options[:bank].id,
            parameters: options[:parameters]                        
          }.to_json
        ){ |response, request, result|   


          puts '---------------Login'
          puts response.code
          puts response.inspect

          case result.code.to_i
          when 202

          when 400
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def login_status(options = {})
    
        options[:sid] ||= @sid

        raise Unnax::Exceptions::UnknownError, "Please login first" unless @bank

        RestClient::Request.execute(
          method: :get,
          url: "#{ base_url }/payment/transfer/lockstep/login/?sid=#{ options[:sid] }"
        ){ |response, request, result| 
          parsed_response = JSON.parse(response.body)

          puts '---------------Login status'
          puts response.code
          puts parsed_response

          case result.code.to_i
          when 200
            parameters = []
            accounts = []
            
            parsed_response.each do |k, v|
              if k == 'parameters'
                v.each do |parameter|
                  parameters << Unnax::BankParameter.new(parameter)
                end
              elsif k == 'account_list'
                v.each do |account|
                  accounts << Unnax::Account.new(account)
                end
              else
                instance_variable_set :"@#{k}", v
              end
            end

            @bank.accounts = accounts
            @bank.parameters = parameters
          
            @retry = false
          when 202
            @retry = true
          when 400
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def select_account(options = {})
    
        options[:sid] ||= @sid
        @account = options[:account]

        RestClient::Request.execute(
          method: :post,
          url: "#{ base_url }/payment/transfer/lockstep/select_account/?sid=#{ options[:sid] }",
          headers: {
            'Content-Type': 'application/json',
          },
          payload: {
            account_id: @account.id                     
          }.to_json
        ){ |response, request, result|

          puts '---------------Select account'
          puts({account_id: @account.id}.to_json)
          puts response.code
          puts response.inspect

          case result.code.to_i
          when 202
            @retry = true
          when 400
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def select_account_status(options = {})
    
        options[:sid] ||= @sid

        RestClient::Request.execute(
          method: :get,
          url: "#{ base_url }/payment/transfer/lockstep/select_account/?sid=#{ options[:sid] }"
        ){ |response, request, result| 

          parsed_response = JSON.parse(response.body)

          puts '---------------Select account status'
          puts response.code
          puts parsed_response

          case result.code.to_i
          when 202
          when 200
            parameters = []
            
            parsed_response.each do |k, v|
              if k == 'parameters'
                v.each do |parameter|
                  parameters << Unnax::AccountParameter.new(parameter)
                end
              else
                instance_variable_set :"@#{k}", v
              end
            end

            @account.parameters = parameters
          
            @retry = false
          when 400
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            parsed_response = JSON.parse(response.body)
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def retry?
        @retry
      end
    end
  end
end