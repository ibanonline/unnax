require 'base64'

module Unnax
  module Requests
    class Base
      class << self
        def authorization_header
          "Unnax #{Base64.encode64("#{Unnax.configuration.api_id}:#{Unnax.configuration.api_code}")}".chomp
        end

        def base_url
          Unnax.configuration.env == 'production' ? 'https://www.unnax.com/api/v3' : 'https://integration.unnax.com/api/v3'
        end
      end

      def base_url
        self.class.base_url
      end

      def authorization_header
        self.class.authorization_header
      end
    end
  end
end
