module Unnax
  module Requests
    class ReaderTokenized < Base

      def initialize

      end

      def self.read(token_key, token_id, request_code, start_date)
        RestClient::Request.execute(
            method: :post,
            url: "#{ base_url }/reader/tokenized/",
            headers: {
                'Authorization': authorization_header,
                'Content-Type': 'application/json'
            },
            payload: {
                token_key: token_key,
                token_id: token_id,
                request_code: request_code,
                start_date: start_date
            }.to_json
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)

          case result.code.to_i
          when 202
            return Unnax::Job.new parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end

      def self.status(job_id)
        RestClient::Request.execute(
            method: :get,
            url: "#{ base_url }/reader/tokenized/#{ job_id }",
            headers: {
                'Authorization': authorization_header
            }
        ){ |response, request, result|
          parsed_response = JSON.parse(response.body)

          case result.code.to_i
          when 200, 202
            return parsed_response
          when 400
            raise Unnax::Exceptions::ValidationError, parsed_response
          when 401
            raise Unnax::Exceptions::Unauthorized, parsed_response['detail']
          else
            raise Unnax::Exceptions::UnknownError, response.to_s
          end
        }
      end
    end
  end
end
