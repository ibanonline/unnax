module Unnax
  class Account

    attr_accessor :id_card
    attr_accessor :full_name

    def initialize(*args)
      if args.first.is_a?(Hash)
        args.first.each do |k, v|
          instance_variable_set :"@#{ k }", v
        end
      end
    end
  end
end