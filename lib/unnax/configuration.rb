module Unnax
  class Configuration
    attr_accessor :base_url, :env, :api_id, :api_code, :callback_url
  end
end