# Unnax

A simple wrapper around [unnax.com](http://unnax.com/).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'unnax'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install unnax

## Usage

```ruby
Unnax.configure do |config|
  config.env = ENV['UNNAX_ENV'] # 'production' : https://www.unnax.com/api/v3, 'sandbox' : https://integration.unnax.com/api/v3
  config.api_id = ENV['UNNAX_API_ID']
  config.api_code = ENV['UNNAX_API_CODE']
  config.callback_url = ENV['UNNAX_CALLBACK_URL']
end
```

### Payment DBT

#### Start

```ruby
client = Unnax::Requests::PaymentDBT.new


client.start amount: 1, concept: 'test', currency: 'EUR'  

#<Unnax::Requests::PaymentDBT:0x007f9cb77a3dd8 @amount=1, @concept="test", @currency="EUR", @order_code="0be0e32506e746111bc87097496e20aa", @products=["cashub_dbt", "check_id", "fitnance"], @sid="s_0c9afffcd18a4260bee5413dbc302837">

begin
  client.start
rescue Unnax::Exceptions::ValidationError => e
  #<Unnax::Exceptions::ValidationError: @code="E000016" @status="error" @message="This product is not enabled for the merchant" @data="">
  # or 
  #<Unnax::Exceptions::ValidationError: @code="" @status="fail" @message="" @data="{"concept"=>"Este campo no puede estar en blanco."}">
rescue Unnax::Exceptions::Unauthorized => e
  #<Unnax::Exceptions::Unauthorized: Invalid token.>
end
```

#### Available banks

```ruby
client = Unnax::Requests::PaymentDBT.new
client.start amount: 1, concept: 'test', currency: 'EUR'  
client.get_banks
client.banks

# or if you have sid from previous request

client = Unnax::Requests::PaymentDBT.new
client.start amount: 1, concept: 'test', currency: 'EUR' 
 
client2.get_banks sid: client.sid
client2.banks

begin
  client.start
rescue Unnax::Exceptions::ValidationError => e
  #<Unnax::Exceptions::ValidationError: @code="" @status="fail" @message="" @data="{"sid"=>"Session is not valid!"}">
rescue Unnax::Exceptions::Unauthorized => e
  #<Unnax::Exceptions::Unauthorized: Invalid token.>
end
```

#### Login

```ruby
client = Unnax::Requests::PaymentDBT.new
client.start amount: 1, concept: 'test', currency: 'EUR'  
client.get_banks
client.login bank_id: client.banks[0].id, parameters: {username: 'test', password: 'test'}

# or if you have sid from previous request

client = Unnax::Requests::PaymentDBT.new
client.start amount: 1, concept: 'test', currency: 'EUR'  
client.get_banks
client2 = Unnax::Requests::PaymentDBT.new
client2.login sid: client.sid, bank_id: client.banks[0].id, parameters: {username: 'test', password: 'test'}

begin
  client.start
rescue Unnax::Exceptions::ValidationError => e
  #<Unnax::Exceptions::ValidationError: @code="" @status="fail" @message="" @data="{"sid"=>"Session is not valid!"}">
rescue Unnax::Exceptions::Unauthorized => e
  #<Unnax::Exceptions::Unauthorized: Invalid token.>
end
```

### Webhooks

#### List

```ruby
activated_webhooks = Unnax::Requests::Webhooks.list

#<Unnax::Webhooks:0x007fab05f4b8e0 @count=1, @results=[#<Unnax::Webhook:0x007fab05f4b728 @id=89, @client="email", @event="fitnance_read", @target="test@mail.com", @created_at=Tue, 03 Apr 2018 14:40:13 +0000, @updated_at=Tue, 03 Apr 2018 14:40:13 +0000>], @next=nil, @previous=nil>

activated_webhooks.results.each do |webhook|
  # ...
end
```

#### Create

```ruby
webhook = Unnax::Requests::Webhooks.create email: 'test@gmail.com', event: 'test', target: 'test'

#<Unnax::Webhook:0x007fab05f4b728 @id=89, @client="email", @event="fitnance_read", @target="test@mail.com", @created_at=Tue, 03 Apr 2018 14:40:13 +0000, @updated_at=Tue, 03 Apr 2018 14:40:13 +0000>

# validation error

#Unnax::Exceptions::ValidationError: {"event"=>"\"test\" no es una elección válida.", "client"=>"\"test@gmail.com\" no es una elección válida."}

begin
  webhook = Unnax::Requests::Webhooks.create
rescue Unnax::Exceptions::ValidationError => e
  e.data # {"event"=>"\"test\" no es una elección válida.", "client"=>"\"test@gmail.com\" no es una elección válida."}
end


```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/unnax. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Unnax project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/unnax/blob/master/CODE_OF_CONDUCT.md).
