require 'rest-client'
require 'json'
require 'spec_helper'

RSpec.describe Unnax::Requests::PaymentDBT do

  before :all do
    Unnax.configure do |config|
      config.env = 'production'
      config.api_id = 'test'
      config.api_code = 'test'
    end
  end

  describe 'start' do
    it "success" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/init/").
        with(headers: {
          'Authorization' => Unnax::Requests::Base.authorization_header
        }).
        to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_start.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.start amount: 1, concept: 'test', currency: 'EUR'   

      expect(client.amount).to eq 1
      expect(client.concept).to eq 'test'
      expect(client.currency).to eq 'EUR'
  
      expect(client.sid).to_not be nil
      expect(client.products).to be_a Array
      expect(client.products.size).to eq 3
    end

    it "validation error" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/init/").
        with(headers: {
          'Authorization' => Unnax::Requests::Base.authorization_header
        }).
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_start_validation.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.start amount: 1, concept: 'test', currency: 'EUR'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.data['concept']).to eq "Este campo no puede estar en blanco."
        expect(error.status).to eq "fail"
      }
    end

    it "unauthorized" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/init/").
        with(headers: {
          'Authorization' => Unnax::Requests::Base.authorization_header
        }).
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.start amount: 1, concept: 'test', currency: 'EUR'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end

    it "not enabled product" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/init/").
        with(headers: {
          'Authorization' => Unnax::Requests::Base.authorization_header
        }).
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_start_no_product.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.start amount: 1, concept: 'test', currency: 'EUR'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.message).to eq "This product is not enabled for the merchant"
        expect(error.code).to eq "E000016"
        expect(error.status).to eq "error"
      }
    end
  end

  describe 'banks' do
    it "success" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/banks/?sid=test").
        to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_banks.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.get_banks sid: 'test'

      expect(client.banks.count).to eq 28
    end

    it "invalid sid" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/banks/?sid=test").
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_banks_invalid_sid.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.get_banks sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.data['sid']).to eq "Session is not valid!"
        expect(error.status).to eq "fail"
      }
    end

    it "unauthorized" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/banks/?sid=test").
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.get_banks sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end
  end

  describe 'login' do
    it "success" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 202, body: '', headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.login sid: 'test', bank: Unnax::Bank.new(id: 1)
    end

    it "invalid sid" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_banks_invalid_sid.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.login sid: 'test', bank: Unnax::Bank.new(id: 1)
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.data['sid']).to eq "Session is not valid!"
        expect(error.status).to eq "fail"
      }
    end

    it "unauthorized" do
      stub_request(:post, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
  
      expect{
        client.login sid: 'test', bank: Unnax::Bank.new(id: 1)
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end
  end

  describe 'login status' do
    it "success" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_login_status.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1
      client.login_status sid: 'test'

      expect(client.bank.parameters.count).to eq(1)
      expect(client.bank.parameters[0].type).to eq('password')
      expect(client.bank.parameters[0].fields.count).to eq(6)
      expect(client.step2).to eq(true)
    end

    it "validation" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_login_status_validation.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      expect{
        client.login_status sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.message).to eq "Missing parameter: document"
        expect(error.status).to eq "error"
      }
    end

    it "wait" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 202, body: File.read('spec/fixtures/payment_dbt_login_status_wait.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      client.login_status sid: 'test'
      
      expect(client.retry).to eq(true)
    end

    it "unauthorized" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      expect{
        client.login_status sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end
  end

  describe 'account selection' do
    it "success" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_login_status.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1
      client.login_status sid: 'test'

      expect(client.bank.parameters.count).to eq(1)
      expect(client.bank.parameters[0].type).to eq('password')
      expect(client.bank.parameters[0].fields.count).to eq(6)
      expect(client.step2).to eq(true)
    end

    it "unauthorized" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      expect{
        client.login_status sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end
  end

  describe 'account selection status' do
    it "success" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_account_selection_status.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1
      client.login_status sid: 'test'

      expect(client.bank.parameters.count).to eq(1)
      expect(client.bank.parameters[0].type).to eq('password')
      expect(client.bank.parameters[0].fields.count).to eq(6)
      expect(client.step2).to eq(true)
    end

    it "validation" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_account_selection_status_validation.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      expect{
        client.login_status sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::ValidationError
        expect(error.message).to eq "Missing parameter: document"
        expect(error.status).to eq "error"
      }
    end

    it "wait" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 202, body: File.read('spec/fixtures/payment_dbt_account_selection_status_wait.json'), headers: {})

      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      client.login_status sid: 'test'
      
      expect(client.retry).to eq(true)
    end

    it "unauthorized" do
      stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
        to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
      client = Unnax::Requests::PaymentDBT.new
      client.bank = Unnax::Bank.new id: 1

      expect{
        client.login_status sid: 'test'
      }.to raise_error { |error|
        expect(error).to be_a Unnax::Exceptions::Unauthorized
      }
    end
  end

  describe 'aaaa' do
    it "success" do

      begin
        api_id = ''                                                   # new
        api_code = ''                                       # new
      
        Unnax.configure do |config|
          config.env = 'production'
          config.api_id = api_id
          config.api_code = api_code
        end
      
        client = Unnax::Requests::PaymentDBT.new
      
        # puts "Init:"
        client.start amount: 1, concept: 'IBANON-f4cc23f93515', currency: 'EUR'
      
        # puts client.inspect
      
        # puts "Banks list:"
        client.get_banks
      
        # puts client.banks.map{|b| {b.id => b.name}}
      
        bank = client.banks.find{|bank| bank.id == 15}
      
        puts bank.inspect
        # puts "Login: "
      
        client.login bank: bank, parameters: {E: '', B: ''}
      
        should_continue = true
      
        while should_continue
          client.login_status
          sleep 1
          should_continue = false unless client.retry?
        end
      
        # puts "Account selection: "

        client.select_account account: client.bank.accounts.first

        # puts "Account selection status: "

        should_continue = true
      
        while should_continue
          client.select_account_status
          sleep 1
          should_continue = false unless client.retry?
        end

        # puts client.account.parameters
        # puts 'here'

      rescue Exception => e
        raise
        puts "================"
        puts e.inspect
        puts "================"
      end

      # stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
      #   to_return(status: 200, body: File.read('spec/fixtures/payment_dbt_login_status.json'), headers: {})
  
      # client = Unnax::Requests::PaymentDBT.new
      # client.bank = Unnax::Bank.new id: 1
      # client.login_status sid: 'test'

      # expect(client.bank.parameters.count).to eq(1)
      # expect(client.bank.parameters[0].type).to eq('password')
      # expect(client.bank.parameters[0].fields.count).to eq(6)
      # expect(client.step2).to eq(true)
    end

    # it "validation" do
    #   stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
    #     to_return(status: 400, body: File.read('spec/fixtures/payment_dbt_login_status_validation.json'), headers: {})

    #   client = Unnax::Requests::PaymentDBT.new
    #   client.bank = Unnax::Bank.new id: 1

    #   expect{
    #     client.login_status sid: 'test'
    #   }.to raise_error { |error|
    #     expect(error).to be_a Unnax::Exceptions::ValidationError
    #     expect(error.message).to eq "Missing parameter: document"
    #     expect(error.status).to eq "error"
    #   }
    # end

    # it "wait" do
    #   stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
    #     to_return(status: 202, body: File.read('spec/fixtures/payment_dbt_login_status_wait.json'), headers: {})

    #   client = Unnax::Requests::PaymentDBT.new
    #   client.bank = Unnax::Bank.new id: 1

    #   client.login_status sid: 'test'
      
    #   expect(client.retry).to eq(true)
    # end

    # it "unauthorized" do
    #   stub_request(:get, "#{ Unnax::Requests::Base.base_url}/payment/transfer/lockstep/login/?sid=test").
    #     to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})
  
    #   client = Unnax::Requests::PaymentDBT.new
    #   client.bank = Unnax::Bank.new id: 1

    #   expect{
    #     client.login_status sid: 'test'
    #   }.to raise_error { |error|
    #     expect(error).to be_a Unnax::Exceptions::Unauthorized
    #   }
    # end
  end
end