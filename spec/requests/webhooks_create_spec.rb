require 'rest-client'
require 'json'
require 'spec_helper'

RSpec.describe Unnax::Requests::Webhooks do

  before :all do
    Unnax.configure do |config|
      config.env = 'sandbox'
      config.api_id = 'test_api_id'
      config.api_code = 'test_api_code'
    end
  end
 
  it "subscribe to webhook" do
    stub_request(:post, "#{ Unnax::Requests::Base.base_url}/webhooks/").
      with(headers: {
        'Authorization' => Unnax::Requests::Base.authorization_header
      }).
      to_return(status: 201, body: File.read('spec/fixtures/webhooks_create.json'), headers: {})

    result = Unnax::Requests::Webhooks.create email: 'test@gmail.com', event: 'test', target: 'test'      

  
    expect(result.class).to eq Unnax::Webhook
    expect(result.id).to eq 89
    expect(result.client).to eq 'email'
    expect(result.event).to eq 'fitnance_read'
    expect(result.target).to eq 'test@mail.com'
    expect(result.created_at).to eq DateTime.parse('2018-04-03T14:40:13Z')
    expect(result.updated_at).to eq DateTime.parse('2018-04-03T14:40:13Z')
  end

  it "return validation errors" do
    stub_request(:post, "#{ Unnax::Requests::Base.base_url}/webhooks/").
      with(headers: {
        'Authorization' => Unnax::Requests::Base.authorization_header
      }).
      to_return(status: 400, body: File.read('spec/fixtures/webhooks_create_validation_error.json'), headers: {})

    expect{Unnax::Requests::Webhooks.create}.to raise_error do |error|
      expect(error).to be_a Unnax::Exceptions::ValidationError
      expect(error.data['event']).to_not be nil
      expect(error.data['target']).to_not be nil
      expect(error.data['client']).to_not be nil
    end
  end

  it "raise unauthorized error" do
    stub_request(:post, "#{ Unnax::Requests::Base.base_url }/webhooks/").
      with(headers: {
        'Authorization' => Unnax::Requests::Base.authorization_header
      }).
      to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})

    expect{
      Unnax::Requests::Webhooks.create
    }.to raise_error(Unnax::Exceptions::Unauthorized)
  end
end