require 'base64'

RSpec.describe Unnax::Requests::Base do
    it "generates authorization header" do
        api_id = 'test_api_id'
        api_code = 'test_api_code'

        Unnax.configure do |config|
            config.api_id = api_id
            config.api_code = api_code
        end

        base64_part = Unnax::Requests::Base.authorization_header.split(' ').last
        expect(Base64.decode64(base64_part)).to eq "#{api_id}:#{api_code}"
    end

    it "switches between production and sandbox" do
        api_id = 'test_api_id'
        api_code = 'test_api_code'

        Unnax.configure do |config|
            config.env = 'production'
        end

        expect(Unnax::Requests::Base.base_url).to eq 'https://www.unnax.com/api/v3'

        Unnax.configure do |config|
            config.env = 'sandbox'
        end

        expect(Unnax::Requests::Base.base_url).to eq 'https://integration.unnax.com/api/v3'
    end
end