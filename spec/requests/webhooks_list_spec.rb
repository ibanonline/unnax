require 'rest-client'
require 'json'
require 'spec_helper'

RSpec.describe Unnax::Requests::Webhooks do

  before :all do
    Unnax.configure do |config|
      config.env = 'sandbox'
      config.api_id = 'test_api_id'
      config.api_code = 'test_api_code'
    end
  end
 
  it "fetch enabled webhooks" do
    stub_request(:get, "#{ Unnax::Requests::Base.base_url}/webhooks/").
      with(headers: {
        'Authorization' => Unnax::Requests::Base.authorization_header
      }).
      to_return(status: 200, body: File.read('spec/fixtures/webhooks_list.json'), headers: {})

    result = Unnax::Requests::Webhooks.list      

    expect(result.class).to eq Unnax::Webhooks
    expect(result.count).to eq 1
    expect(result.next).to eq nil
    expect(result.previous).to eq nil
    expect(result.results.count).to eq 1
    
  
    expect(result.results[0].class).to eq Unnax::Webhook
    expect(result.results[0].id).to eq 89
    expect(result.results[0].client).to eq 'email'
    expect(result.results[0].event).to eq 'fitnance_read'
    expect(result.results[0].target).to eq 'test@mail.com'
    expect(result.results[0].created_at).to eq DateTime.parse('2018-04-03T14:40:13Z')
    expect(result.results[0].updated_at).to eq DateTime.parse('2018-04-03T14:40:13Z')
  end

  it "raise unauthorized error" do
    stub_request(:get, "#{ Unnax::Requests::Base.base_url }/webhooks/").
      with(headers: {
        'Authorization' => Unnax::Requests::Base.authorization_header
      }).
      to_return(status: 401, body: File.read('spec/fixtures/unauthorized.json'), headers: {})

    expect{
      Unnax::Requests::Webhooks.list
    }.to raise_error(Unnax::Exceptions::Unauthorized)
  end
end